import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import DefaultLayout from './layouts/default.vue'
import VueSelect from "vue-select";
import userAgentPlugin from './plugins/userAgent'
import "vue-select/dist/vue-select.css";

import '/src/assets/global.scss'

// УСТАНОВКА приложения
// Обработчик события beforeinstallprompt
let deferredPrompt;
let installButton;
// Флаг для проверки, была ли кнопка установки показана ранее
let isInstallButtonShown = false;

window.addEventListener('beforeinstallprompt', (event) => {
  installButton = document.querySelector('.install-button')

  // Предотвращаем браузер от вызова диалога установки приложения
  event.preventDefault();

  // Сохраняем объект event в переменную, чтобы использовать его позже
  deferredPrompt = event;

  // Если кнопка установки еще не была показана, показываем ее
  if (!isInstallButtonShown) {
    setTimeout(() => {
      installButton.style.display = 'block';
      isInstallButtonShown = true;
    }, 5000)
  }

  // Обработчик клика по кнопке установки приложения
  installButton.addEventListener('click', () => {
    // Прячем кнопку установки приложения
    installButton.style.display = 'none';

    // Вызываем диалог установки приложения
    deferredPrompt.prompt();

    // Обрабатываем результат диалога установки приложения
    deferredPrompt.userChoice.then((choiceResult) => {
      console.log('deferredPrompt', deferredPrompt)
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the install prompt');
      } else {
        console.log('User dismissed the install prompt');
      }
    });
  });
});

const app = createApp(App)
app.component('VSelect', VueSelect)
app.component('default-layout', DefaultLayout)
app.use(createPinia())
app.use(router)
app.use(userAgentPlugin)

app.mount('#app')