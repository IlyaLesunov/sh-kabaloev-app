// Как пользоваться
// В setup:
// import { inject } from 'vue'
// const isMobile = inject('$isMobile')
// В template:
// $isMobile

export default {
  install: (app) => {
    const userAgent = navigator.userAgent;
    app.config.globalProperties.$isMobile = /iPhone|iPad|iPod|Android/i.test(userAgent);
  }
}