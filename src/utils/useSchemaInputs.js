import { reactive } from 'vue'
import {
  valueFormRequestSale,
  valueFormRequestReviews
} from './dictInputs.js'

export default function() {

  const inputValuesData = reactive({
    success: false,
    review: valueFormRequestReviews,
    application: valueFormRequestSale
  })

  // Меняем значение отправленная форма и обратно
  const changeSuccessForm = () => {
    return inputValuesData.success = !inputValuesData.success
  }

  // Дописать полноценную валидацию input
  const checkInputValidate = (payload) => {
    if (typeof payload.answer === 'string') {
      payload.answer === '' ? payload.valid = true : payload.valid = false
    }
  }
  // Дописать полноценную валидацию input
  const clearAllFields = () => {
    console.log('clearAllFields')
    inputValuesData['review'].forEach((item) => {
      if (typeof item.answer === 'string') {
        item.valid = false
        item.answer = ''
      }
    })
    inputValuesData['application'].forEach((item) => {
      if (typeof item.answer === 'string') {
        item.valid = false
        item.answer = ''
      }
    })
  }

  // Дописать полноценную валидацию input
  const checkAllValidateValue = (key) => {
    const status =
      (inputValuesData[key].every(value => value.answer !== "")) &&
      (inputValuesData[key].every(value => value?.valid !== true))
    if (!status) {
      inputValuesData[key].forEach((item) => {
        if (typeof item.answer === 'string') {
          item.valid = true
        }
      })
    }
    return status
  }

  return {
    sending: inputValuesData.success,
    reviewsValueInput: inputValuesData.review,
    applicationValueInput: inputValuesData.application,
    checkInputValidate,
    checkAllValidateValue,
    clearAllFields,
    changeSuccessForm,
  }
}
