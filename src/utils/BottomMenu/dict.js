import house from '@/assets/images/icons/menu/house.svg'
import price from '@/assets/images/icons/menu/price.svg'
import reviews from  '@/assets/images/icons/menu/reviews.svg'
import contacts from '@/assets/images/icons/menu/contacts.svg'


const menu = [
  {
    title: 'Главная',
    url: '/',
    icon: house,
    active: true
  },
  {
    title: 'Прайс',
    url: '/price',
    icon: price,
    active: false
  },
  {
    title: 'Отзывы',
    url: '/reviews',
    icon:  reviews,
    active: false
  },
  {
    title: 'Контакты',
    url: '/contacts',
    icon: contacts,
    active: false
  }
]

export default menu
