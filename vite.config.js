import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { VitePWA } from 'vite-plugin-pwa'
// import copy from 'rollup-plugin-copy';

// https://vitejs.dev/config/
export default defineConfig({
  base: '/sh-kabaloev-app',
  plugins: [
    vue(),
    VitePWA()
    // copy({
    //   verbose: true,
    //   targets: [
    //     { src: 'src/assets/images/**/*', dest: 'dist/assets/images/' }
    //   ]
    // })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  // output: {
  //   dir: 'dist',
  //   assetDir: 'images',
  // }
})